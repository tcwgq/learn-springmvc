package com.tcwgq.po;

import java.util.List;
import java.util.Map;

public class ItemsQueryVo {

	// 商品信息
	private Items items;

	// 为了系统 可扩展性，对原始生成的po进行扩展
	private ItemsCustom itemsCustom;

	// 批量修改商品
	private List<ItemsCustom> itemsList;

	private Map<String, Object> itemsMap;

	public Items getItems() {
		return items;
	}

	public void setItems(Items items) {
		this.items = items;
	}

	public ItemsCustom getItemsCustom() {
		return itemsCustom;
	}

	public void setItemsCustom(ItemsCustom itemsCustom) {
		this.itemsCustom = itemsCustom;
	}

	public List<ItemsCustom> getItemsList() {
		return itemsList;
	}

	public void setItemsList(List<ItemsCustom> itemsList) {
		this.itemsList = itemsList;
	}

	public Map<String, Object> getItemsMap() {
		return itemsMap;
	}

	public void setItemsMap(Map<String, Object> itemsMap) {
		this.itemsMap = itemsMap;
	}

	@Override
	public String toString() {
		return "ItemsQueryVo [items=" + items + ", itemsCustom=" + itemsCustom + ", itemsList=" + itemsList + ", itemsMap=" + itemsMap + "]";
	}

}
