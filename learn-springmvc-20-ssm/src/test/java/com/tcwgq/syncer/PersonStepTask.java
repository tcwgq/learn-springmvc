package com.tcwgq.syncer;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tcwgq.mapper.PersonMapper;
import com.tcwgq.mapper.PersonPlusMapper;
import com.tcwgq.po.Person;
import com.tcwgq.po.PersonCondition;
import com.tcwgq.po.PersonPlus;

/**
 * @author tcwgq
 * @time 2017年10月26日 上午8:21:33
 * @email tcwgq@outlook.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class PersonStepTask {
	private static final Integer BATCH_SIZE = 10;

	@Autowired
	private PersonMapper personMapper;

	@Autowired
	private PersonPlusMapper personPlusMapper;

	public void sync() {
		int totalCount = 0;
		int pageNo = 0;
		do {
			int offset = pageNo * BATCH_SIZE;
			int pageSize = BATCH_SIZE;

			PersonCondition condition = new PersonCondition();
			condition.setOffset(offset);
			condition.setPageSize(pageSize);

			List<Person> persons = personMapper.find(condition);
			totalCount = persons.size();

			for (Person person : persons) {
				PersonPlus personPlus = new PersonPlus();
				personPlus.setName(person.getName());
				personPlusMapper.insert(personPlus);
			}

			pageNo++;
		} while (totalCount > 0);
	}

	@Test
	public void test1() {
		sync();
	}
}
