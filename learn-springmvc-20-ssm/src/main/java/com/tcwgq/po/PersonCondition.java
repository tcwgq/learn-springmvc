package com.tcwgq.po;

import java.util.Date;

/**
 * @author tcwgq
 * @time 2017年10月25日 下午10:10:05
 * @email tcwgq@outlook.com
 */
public class PersonCondition {
	private Integer id;
	private String name;
	private Integer age;
	private Date timeCreatedStart;
	private Date timeCreatedEnd;
	private Integer offset;
	private Integer pageSize;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getTimeCreatedStart() {
		return timeCreatedStart;
	}

	public void setTimeCreatedStart(Date timeCreatedStart) {
		this.timeCreatedStart = timeCreatedStart;
	}

	public Date getTimeCreatedEnd() {
		return timeCreatedEnd;
	}

	public void setTimeCreatedEnd(Date timeCreatedEnd) {
		this.timeCreatedEnd = timeCreatedEnd;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "UserCondition [id=" + id + ", name=" + name + ", age=" + age + ", timeCreatedStart=" + timeCreatedStart + ", timeCreatedEnd=" + timeCreatedEnd
				+ ", offset=" + offset + ", pageSize=" + pageSize + "]";
	}

}
