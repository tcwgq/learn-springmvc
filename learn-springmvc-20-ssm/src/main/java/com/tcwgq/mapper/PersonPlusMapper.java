package com.tcwgq.mapper;

import org.springframework.stereotype.Repository;

import com.tcwgq.po.PersonPlus;

/**
 * @author tcwgq
 * @time 2017年10月25日 下午10:46:51
 * @email tcwgq@outlook.com
 */
@Repository
public interface PersonPlusMapper {
	public int insert(PersonPlus personPlus);

	public PersonPlus get(Integer id);

	public int delete(Integer id);
}
