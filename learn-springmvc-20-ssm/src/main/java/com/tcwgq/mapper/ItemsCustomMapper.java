package com.tcwgq.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tcwgq.po.ItemsCustom;
import com.tcwgq.po.ItemsQueryVo;

@Repository
public interface ItemsCustomMapper {

	public List<ItemsCustom> findItemsList(ItemsQueryVo itemsQueryVo) throws Exception;

}