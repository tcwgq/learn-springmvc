package com.tcwgq.sync.task;

import java.util.concurrent.*;

/**
 * @author tcwgq
 * @time 2017年10月26日 上午8:19:03
 * @email tcwgq@outlook.com
 */
public class ThreadPoolSyncer {
    private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(
            100,
            100,
            1L,
            TimeUnit.MINUTES,
            new ArrayBlockingQueue<>(1000),
            Executors.defaultThreadFactory());

    private ThreadPoolSyncer() {

    }

    public static ExecutorService getInstance() {
        return EXECUTOR;
    }
}
