package com.tcwgq.exception;

/**
 * @author tcwgq
 * @time 2017年10月17日 下午9:41:04
 * @email tcwgq@outlook.com
 */
public class CustomException extends Exception {

	public CustomException() {
		super();
	}

	public CustomException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CustomException(String message, Throwable cause) {
		super(message, cause);
	}

	public CustomException(String message) {
		super(message);
	}

	public CustomException(Throwable cause) {
		super(cause);
	}

}
