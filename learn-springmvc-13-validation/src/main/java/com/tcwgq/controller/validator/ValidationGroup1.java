package com.tcwgq.controller.validator;

/**
 * @author tcwgq
 * @time 2017年10月16日 下午10:08:00
 * @email tcwgq@outlook.com
 */
public interface ValidationGroup1 {
	// 接口中不需要定义任何方法，只是对不同的规则进行分组
	// 此分组只校验商品名称的长度
}
