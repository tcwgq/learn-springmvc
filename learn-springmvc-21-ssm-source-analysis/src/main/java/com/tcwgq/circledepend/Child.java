package com.tcwgq.circledepend;

/**
 * @author tcwgq
 * @time 2020/9/3 11:43
 */
public class Child {
    private Parent parent;

    public Child() {
    }

    public Child(Parent parent) {
        this.parent = parent;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }
}
