package com.tcwgq.circledepend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tcwgq
 * @time 2020/9/2 21:47
 */
@Service
public class B {
    @Autowired
    private A a;
}
