package com.tcwgq.mapper;

import com.tcwgq.model.User;
import com.tcwgq.po.UserVo;

import java.util.List;

/**
 * @author tcwgq
 * @time 2017年9月13日下午9:58:05
 * @email tcwgq@outlook.com
 */
public interface UserMapper {

    public int insertUser(User user) throws Exception;

    public User getById(Integer id) throws Exception;

    public List<User> getByName(String name) throws Exception;

    public List<User> find(UserVo vo) throws Exception;

    public int updateUser(User user) throws Exception;

    public int deleteById(Integer id) throws Exception;
}
