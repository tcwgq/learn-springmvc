package com.tcwgq.mapper;

import com.tcwgq.po.ItemsCustom;
import com.tcwgq.po.ItemsQueryVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemsCustomMapper {

	public List<ItemsCustom> findItemsList(ItemsQueryVo itemsQueryVo) throws Exception;

}