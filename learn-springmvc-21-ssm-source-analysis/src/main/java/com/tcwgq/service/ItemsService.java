package com.tcwgq.service;

import com.tcwgq.po.ItemsCustom;
import com.tcwgq.po.ItemsQueryVo;

import java.util.List;

public interface ItemsService {

	public List<ItemsCustom> findItemsList(ItemsQueryVo itemsQueryVo) throws Exception;

	public ItemsCustom findItemsById(Integer id) throws Exception;

	public void updateItems(Integer id, ItemsCustom itemsCustom) throws Exception;

}
