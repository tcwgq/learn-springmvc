package com.tcwgq.mybatis;

import com.tcwgq.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * 基于xml配置文件形式
 *
 * @author tcwgq
 * @time 2017年9月10日上午9:58:45
 * @email tcwgq@outlook.com
 */
public class MybatisTest {
    @Test
    public void testGetById() throws IOException {
        // 不能加classpath前缀
        InputStream is = Resources.getResourceAsStream("mybatis-config-test.xml");
        // Configuration config = new Configuration();
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        SqlSession session = factory.openSession();
        // 命名空间.statement的id
        User user = session.selectOne("user.getById", 10);
        System.out.println(user);
        session.close();
        is.close();
    }

    @Test
    public void testGetByName() throws IOException {
        // 不能加classpath前缀
        InputStream is = Resources.getResourceAsStream("mybatis-config-test.xml");
        // Configuration config = new Configuration();
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        SqlSession session = factory.openSession();
        // 命名空间.statement的id
        List<User> list = session.selectList("user.getByName", "小明");
        System.out.println(list);
        session.close();
        is.close();
    }

    @Test
    public void testInsertUser() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        // Configuration config = new Configuration();
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        SqlSession session = factory.openSession();
        // 命名空间.statement的id
        User user = new User();
        user.setUsername("zhangSan");
        user.setBirthday(new Date());
        user.setSex("m");
        user.setAddress("shandong");
        int rows = session.insert("user.insertUser", user);
        // 此处需要手动提交事务
        session.commit();
        System.out.println(rows);
        System.out.println(user.getId());
        session.close();
        is.close();
    }

    @Test
    public void testDeleteUser() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        // Configuration config = new Configuration();
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        SqlSession session = factory.openSession();
        // 命名空间.statement的id
        int rows = session.delete("user.deleteUserById", 28);
        // 此处需要手动提交事务
        session.commit();
        System.out.println(rows);
        session.close();
        is.close();
    }

    @Test
    public void testUpdateUser() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        // Configuration config = new Configuration();
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        SqlSession session = factory.openSession();
        // 命名空间.statement的id
        User user = new User();
        user.setId(26);
        user.setUsername("zhangSan");
        user.setBirthday(new Date());
        user.setSex("m");
        user.setAddress("shandong");
        int rows = session.insert("user.updateUser", user);
        // 此处需要手动提交事务
        session.commit();
        System.out.println(rows);
        System.out.println(user.getId());
        session.close();
        is.close();
    }
}
