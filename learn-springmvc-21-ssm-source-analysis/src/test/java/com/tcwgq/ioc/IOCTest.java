package com.tcwgq.ioc;

import com.tcwgq.beanaware.Person;
import com.tcwgq.circledepend.Parent;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * @author tcwgq
 * @time 2017年8月13日下午10:27:42
 * @email tcwgq@outlook.com
 */
public class IOCTest {
    @Test
    public void test1() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{"classpath:applicationContext-test.xml"});
        Person person = ctx.getBean("person", Person.class);
        person.add();
        System.out.println(person);
        ctx.registerShutdownHook();
    }

    @Test
    public void test2() {
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("applicationContext-test.xml"));
        Person person = (Person) beanFactory.getBean("person");
        person.add();
    }

    @Test
    public void test3() {
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("applicationContext-circledepend.xml"));
        Parent person = (Parent) beanFactory.getBean("parent");
    }
}
