package com.tcwgq.mapper;

import com.tcwgq.model.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 整合spring
 *
 * @author tcwgq
 * @time 2020/8/18 22:08
 */
public class UserMapperTest {

    @Test
    public void insertUser() throws Exception {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        UserMapper userMapper = (UserMapper) ctx.getBean("userMapper");
        User user = userMapper.getById(10);
        System.out.println(user);
    }

    @Test
    public void getById() {
    }

    @Test
    public void getByName() {
    }

    @Test
    public void updateUser() {
    }

    @Test
    public void deleteById() {
    }
}