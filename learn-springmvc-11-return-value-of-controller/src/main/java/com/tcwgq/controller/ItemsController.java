package com.tcwgq.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tcwgq.po.ItemsCustom;
import com.tcwgq.service.ItemsService;

/**
 * Controller的返回值
 * 
 * @author tcwgq
 * @time 2017年10月1日 上午10:07:01
 * @email tcwgq@outlook.com
 */
@Controller
// 指定公共前缀
@RequestMapping("/items")
public class ItemsController {

	@Autowired
	private ItemsService service;

	// 返回ModelAndView
	@RequestMapping("/queryItems")
	public ModelAndView queryItems(HttpServletRequest request) throws Exception {
		// 测试转发数据
		System.out.println(request.getParameter("id"));

		List<ItemsCustom> itemsList = service.findItemsList(null);

		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("itemsList", itemsList);
		modelAndView.setViewName("/items/itemsList");
		return modelAndView;
	}

	// 返回String，表示逻辑视图名
	@RequestMapping(value = "/editItems", method = { RequestMethod.GET, RequestMethod.POST })
	public String editItems(Model model, Integer id) throws Exception {
		ItemsCustom itemsCustom = service.findItemsById(id);

		model.addAttribute("itemsCustom", itemsCustom);

		return "/items/editItems";
	}

	// 返回String，修改成功后，重定向到list页面
	@RequestMapping("/editItemsSubmit")
	public String editItemsSubmit(HttpServletRequest request) throws Exception {

		return "forward:queryItems.action";
	}

	// 返回String，修改成功后，重定向到list页面，浏览器地址发生改变
	@RequestMapping("/editItemsRedirect")
	public String editItemsRedirect(HttpServletRequest request) throws Exception {

		return "redirect:queryItems.action";
	}

	// 返回String，修改成功后，转发到list页面，可以共享request域
	@RequestMapping("/editItemsForward")
	public String editItemsForward(HttpServletRequest request) throws Exception {

		return "forward:queryItems.action";
	}

	// 返回void，可以实现原始的Servlet的功能
	@RequestMapping("/getJson")
	public void getJson(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setCharacterEncoding("utf-8");
		response.getWriter().print("Helloworld");
	}
}
