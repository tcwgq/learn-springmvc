package com.tcwgq.factory;

import com.tcwgq.model.Teacher;

/**
 * @author tcwgq
 * @time 2017年8月14日上午8:30:33
 * @email tcwgq@outlook.com
 */
public class TeacherFactory {
	public Teacher getTeacher() {
		return new Teacher();
	}
}
