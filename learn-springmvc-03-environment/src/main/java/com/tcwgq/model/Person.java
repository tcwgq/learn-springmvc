package com.tcwgq.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author tcwgq
 * @time 2017年8月15日下午9:04:06
 * @email tcwgq@outlook.com
 */
public class Person {
	private String name;
	private String[] colors;
	private List<String> list = new ArrayList<>();
	private Map<String, Object> map;
	private Properties props;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getColors() {
		return colors;
	}

	public void setColors(String[] colors) {
		this.colors = colors;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", colors=" + Arrays.toString(colors) + ", list=" + list + ", map=" + map + ", props=" + props + "]";
	}

}
